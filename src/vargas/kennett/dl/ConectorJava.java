package vargas.kennett.dl;

import vargas.kennett.util.Utils;

import java.sql.SQLException;

public class ConectorJava {
    private static AccesoBdSql connectorBD;

    public static AccesoBdSql getConnector() throws Exception {
        String[] infoBd = Utils.getProperties();
        String url =infoBd[0] + "//" +infoBd[1] + "/" + infoBd[2] + "?useSSL=false&serverTimezone=UTC";
        String user = infoBd[3];
        String password = infoBd[4];

        if(connectorBD == null){
            connectorBD = new AccesoBdSql(url,user,password);
        }

        return connectorBD;
    }
}
