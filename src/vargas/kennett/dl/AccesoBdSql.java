package vargas.kennett.dl;

import java.sql.*;

public class AccesoBdSql {
    private Connection conn;
    private Statement stmt;

    // Constructor que inicializar la conexión con la BD
    public AccesoBdSql(String url, String user, String password)throws SQLException {
        conn = DriverManager.getConnection(url,user,password);
    }

    public AccesoBdSql() {

    }

    // Método para realizar consultas a la BD (SELECT)
    public ResultSet ejecutarSQL(String sqlQuery) throws SQLException{
        ResultSet rs = null;
        stmt = conn.createStatement();
        rs = stmt.executeQuery(sqlQuery);

        return rs;
    }

    // Método para realizar otras sentencias CRUD (registar, actualizar, eliminar)
    public void ejecutarQuery(String sqlQuery) throws SQLException{
        stmt = conn.createStatement();
        stmt.executeUpdate(sqlQuery);
    }

}
