package vargas.kennett.dl;

import vargas.kennett.bl.entities.persona.Persona;

import java.util.ArrayList;

public class Data {

    private ArrayList<Persona> administradores;

    public Data(){
        administradores=new ArrayList<Persona>();
    }

    public ArrayList<Persona> getAdmins(){
        return (ArrayList) administradores.clone();
    }

    public void registrarUsuarioAdmin(Persona persona){
        administradores.add(persona);
    }


}
