package vargas.kennett;

import vargas.kennett.tl.Controller;

import java.io.IOException;

public class Principal {

    public static void main (String[] args) throws Exception {

        Controller controller = new Controller();
        controller.start();
    }
}
