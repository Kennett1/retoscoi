package vargas.kennett.ui;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;

public class UI {

    private PrintStream out = System.out;
    private BufferedReader in = new BufferedReader( new InputStreamReader(System.in));

    public void selectionTipeUser () {
        out.println("Bienvenido a la aplicacion que te impulsa a hacer ejercicio");
        out.println("1. Administrador ");
        out.println("2.  Deportista");
        out.println("3. Salir ");
        out.print("Porfavor indique su tipo de usuario");
    }


    public int chooseOption () throws IOException {
        int option;
        option = Integer.parseInt(in.readLine());
       // int option1 = 1;
        //int option2 = 2;
        //int option3 = 3;
        return option;

    }

    public void printMessage (String mensaje) {
        out.println(mensaje);
    }

    public String readText () throws IOException {
        return in.readLine();
    }
}
