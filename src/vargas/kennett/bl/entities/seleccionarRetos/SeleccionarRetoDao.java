package vargas.kennett.bl.entities.seleccionarRetos;

import java.util.ArrayList;

public interface SeleccionarRetoDao {
    public String registrarSeleccionarReto () throws Exception;
    public ArrayList<SeleccionarReto> listarSeleccionarReto() throws Exception;
    public String modificarSeleccionarReto(SeleccionarReto seleccionarReto) throws Exception;
    public String eliminarSeleccionarReto(String codigo) throws Exception;
}
