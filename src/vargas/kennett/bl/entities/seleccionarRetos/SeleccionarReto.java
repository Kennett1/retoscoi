package vargas.kennett.bl.entities.seleccionarRetos;

public class SeleccionarReto {
    private String retoSeleccionado;
    private String formatoDeReto;

    public SeleccionarReto() {
    }

    public SeleccionarReto(String retoSeleccionado, String formatoDeReto) {
        this.retoSeleccionado = retoSeleccionado;
        this.formatoDeReto = formatoDeReto;
    }

    public String getRetoSeleccionado() {
        return retoSeleccionado;
    }

    public void setRetoSeleccionado(String retoSeleccionado) {
        this.retoSeleccionado = retoSeleccionado;
    }

    public String getFormatoDeReto() {
        return formatoDeReto;
    }

    public void setFormatoDeReto(String formatoDeReto) {
        this.formatoDeReto = formatoDeReto;
    }

    @Override
    public String toString() {
        return "SeleccionarReto{" +
                "retoSeleccionado='" + retoSeleccionado + '\'' +
                ", formatoDeReto='" + formatoDeReto + '\'' +
                '}';
    }
}
