package vargas.kennett.bl.entities.retoIndividual;

public class RetoIndividual {
    private int semanas;
    private double montoCancelar;

    public RetoIndividual() {
    }

    public RetoIndividual(int semanas, double montoCancelar) {
        this.semanas = semanas;
        this.montoCancelar = montoCancelar;
    }

    public int getSemanas() {
        return semanas;
    }

    public void setSemanas(int semanas) {
        this.semanas = semanas;
    }

    public double getMontoCancelar() {
        return montoCancelar;
    }

    public void setMontoCancelar(double montoCancelar) {
        this.montoCancelar = montoCancelar;
    }

    @Override
    public String toString() {
        return "RetoIndividual{" +
                "semanas=" + semanas +
                ", montoCancelar=" + montoCancelar +
                '}';
    }
}
