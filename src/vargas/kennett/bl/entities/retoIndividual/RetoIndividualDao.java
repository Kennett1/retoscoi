package vargas.kennett.bl.entities.retoIndividual;

import java.util.ArrayList;

public interface RetoIndividualDao {

    public String registrarRetoIndividual () throws Exception;
    public ArrayList<RetoIndividual> listarRetoIndividual() throws Exception;
    public String modificarRetoIndividual(RetoIndividual retoIndividual) throws Exception;
    public String eliminarRetoIndividual(String codigo) throws Exception;

}
