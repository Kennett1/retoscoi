package vargas.kennett.bl.entities.retoGrupal;

import java.util.ArrayList;

public interface RetoGrupalDao {

    public String registrarRetoGrupal () throws Exception;
    public ArrayList<RetoGrupal> listarRetoGrupal() throws Exception;
    public String modificarRetoGrupal(RetoGrupal retoGrupal) throws Exception;
    public String eliminarRetoGrupal(String codigo) throws Exception;

}
