package vargas.kennett.bl.entities.retoGrupal;

public class RetoGrupal {

    private String nombreGrupo;
    private String imagenGrupo;
    private String correosInvitacion;
    private String enviarInvitacion;
    private String inicializada;

    public RetoGrupal() {
    }

    public RetoGrupal(String nombreGrupo, String imagenGrupo, String correosInvitacion, String enviarInvitacion, String inicializada) {
        this.nombreGrupo = nombreGrupo;
        this.imagenGrupo = imagenGrupo;
        this.correosInvitacion = correosInvitacion;
        this.enviarInvitacion = enviarInvitacion;
        this.inicializada = inicializada;
    }

    public String getNombreGrupo() {
        return nombreGrupo;
    }

    public void setNombreGrupo(String nombreGrupo) {
        this.nombreGrupo = nombreGrupo;
    }

    public String getImagenGrupo() {
        return imagenGrupo;
    }

    public void setImagenGrupo(String imagenGrupo) {
        this.imagenGrupo = imagenGrupo;
    }

    public String getCorreosInvitacion() {
        return correosInvitacion;
    }

    public void setCorreosInvitacion(String correosInvitacion) {
        this.correosInvitacion = correosInvitacion;
    }

    public String getEnviarInvitacion() {
        return enviarInvitacion;
    }

    public void setEnviarInvitacion(String enviarInvitacion) {
        this.enviarInvitacion = enviarInvitacion;
    }

    public String getInicializada() {
        return inicializada;
    }

    public void setInicializada(String inicializada) {
        this.inicializada = inicializada;
    }

    @Override
    public String toString() {
        return "RetoGrupal{" +
                "nombreGrupo='" + nombreGrupo + '\'' +
                ", imagenGrupo='" + imagenGrupo + '\'' +
                ", correosInvitacion='" + correosInvitacion + '\'' +
                ", enviarInvitacion='" + enviarInvitacion + '\'' +
                ", inicializada='" + inicializada + '\'' +
                '}';
    }
}
