package vargas.kennett.bl.entities.tipo;

import java.util.Arrays;

public class Tipo {
    private String tipo;
    private String [] listAdmin;
    private String [] listUsuario;

    public Tipo() {
;
    }

    public Tipo(String tipo, String[] listAdmin, String[] listUsuario) {
        this.tipo = tipo;
        this.listAdmin = listAdmin;
        this.listUsuario = listUsuario;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String[] getListAdmin() {
        return listAdmin;
    }

    public void setListAdmin(String[] listAdmin) {
        this.listAdmin = listAdmin;
    }

    public String[] getListUsuario() {
        return listUsuario;
    }

    public void setListUsuario(String[] listUsuario) {
        this.listUsuario = listUsuario;
    }

    @Override
    public String toString() {
        return "Tipo{" +
                "tipo='" + tipo + '\'' +
                ", listAdmin=" + Arrays.toString(listAdmin) +
                ", listUsuario=" + Arrays.toString(listUsuario) +
                '}';
    }
}
