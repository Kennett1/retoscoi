package vargas.kennett.bl.entities.retos;


import vargas.kennett.dl.ConectorJava;

import java.sql.ResultSet;
import java.util.ArrayList;

public class MySqlIRetosImplement implements IRetosDao {

    private String sqlQuery;

    @Override
    public String registrarRetos(Retos retos) throws Exception {
        sqlQuery = "INSERT INTO retos(codigoReto,nombreReto,descripcionReto,fotoDelReto,cantidadDeKilometros,medallaAsociada,costoDelReto,puntoInicioReto,puntoFinReto,tiempoDeEntregaMedalla) VALUES('"+ retos.getCodigoReto() +"','"+retos.getNombreReto()+"','"+retos.getDescripcionReto()+"','"+retos.getFotoDelReto()+"','"+retos.getCantidadDeKilometros()+"','"+retos.getMedallaAsociada()+"','"+retos.getCostoDelReto()+"','"+retos.getPuntoInicioReto()+"','"+retos.getPuntoFinReto()+"','"+retos.getTiempoDeEntregaMedalla()+"')";
        ConectorJava.getConnector().ejecutarQuery(sqlQuery);
        return "Se registró exitosamente";
    }

    @Override
    public ArrayList<Retos> listarRetos() throws Exception {
        ArrayList<Retos> listarRetos = new ArrayList<>();
        String sqlQueryPersona = "";

        sqlQuery ="SELECT P.nombre, P.apellido, P.identificacion, P.pais, P.crontrasenna\n" +
                "FROM persona P\n";
        ResultSet rsRetos = ConectorJava.getConnector().ejecutarSQL(sqlQuery);

        while(rsRetos.next()){
            Retos retos = new Retos(rsRetos.getInt("codigoReto"),rsRetos.getString("nombreReto"),rsRetos.getString("descripcionReto"), rsRetos.getString("fotoDelReto"),rsRetos.getDouble("cantidadDeKilometros"),rsRetos.getString("medallaAsociada"),rsRetos.getDouble("costoDelReto"),rsRetos.getDouble("puntoInicioReto"),rsRetos.getDouble("puntoFinReto"),rsRetos.getString("tiempoDeEntregaMedalla"));

            listarRetos().add(retos);
        }

        return listarRetos();
    }

    @Override
    public String modificarRetos(Retos retos) throws Exception {
        return null;
    }

    @Override
    public String eliminarRetos(String codigo) throws Exception {
        return null;
    }


}
