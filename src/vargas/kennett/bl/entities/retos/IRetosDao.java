package vargas.kennett.bl.entities.retos;

import java.util.ArrayList;

public interface IRetosDao {

    public String registrarRetos (Retos retos) throws Exception;
    public ArrayList<Retos> listarRetos() throws Exception;
    public String modificarRetos(Retos retos) throws Exception;
    public String eliminarRetos(String codigo) throws Exception;

}
