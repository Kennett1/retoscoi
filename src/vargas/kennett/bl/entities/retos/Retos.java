package vargas.kennett.bl.entities.retos;

public class Retos {

    private int codigoReto;
    private String nombreReto;
    private String descripcionReto;
    private String fotoDelReto;
    private double cantidadDeKilometros;
    private String medallaAsociada;
    private double costoDelReto;
    private double puntoInicioReto;
    private double puntoFinReto;
    private String tiempoDeEntregaMedalla;



    public Retos(int codigoReto, String nombreReto, String descripcionReto, String fotoDelReto, double cantidadDeKilometros, String medallaAsociada, double costoDelReto, double puntoInicioReto, double puntoFinReto, String tiempoDeEntregaMedalla) {
        this.codigoReto = codigoReto;
        this.nombreReto = nombreReto;
        this.descripcionReto = descripcionReto;
        this.fotoDelReto = fotoDelReto;
        this.cantidadDeKilometros = cantidadDeKilometros;
        this.medallaAsociada = medallaAsociada;
        this.costoDelReto = costoDelReto;
        this.puntoInicioReto = puntoInicioReto;
        this.puntoFinReto = puntoFinReto;
        this.tiempoDeEntregaMedalla = tiempoDeEntregaMedalla;
    }

    public int getCodigoReto() {
        return codigoReto;
    }

    public void setCodigoReto(int codigoReto) {
        this.codigoReto = codigoReto;
    }

    public String getNombreReto() {
        return nombreReto;
    }

    public void setNombreReto(String nombreReto) {
        this.nombreReto = nombreReto;
    }

    public String getDescripcionReto() {
        return descripcionReto;
    }

    public void setDescripcionReto(String descripcionReto) {
        this.descripcionReto = descripcionReto;
    }

    public String getFotoDelReto() {
        return fotoDelReto;
    }

    public void setFotoDelReto(String fotoDelReto) {
        this.fotoDelReto = fotoDelReto;
    }

    public double getCantidadDeKilometros() {
        return cantidadDeKilometros;
    }

    public void setCantidadDeKilometros(double cantidadDeKilometros) {
        this.cantidadDeKilometros = cantidadDeKilometros;
    }

    public String getMedallaAsociada() {
        return medallaAsociada;
    }

    public void setMedallaAsociada(String medallaAsociada) {
        this.medallaAsociada = medallaAsociada;
    }

    public double getCostoDelReto() {
        return costoDelReto;
    }

    public void setCostoDelReto(double costoDelReto) {
        this.costoDelReto = costoDelReto;
    }

    public double getPuntoInicioReto() {
        return puntoInicioReto;
    }

    public void setPuntoInicioReto(double puntoInicioReto) {
        this.puntoInicioReto = puntoInicioReto;
    }

    public double getPuntoFinReto() {
        return puntoFinReto;
    }

    public void setPuntoFinReto(double puntoFinReto) {
        this.puntoFinReto = puntoFinReto;
    }

    public String getTiempoDeEntregaMedalla() {
        return tiempoDeEntregaMedalla;
    }

    public void setTiempoDeEntregaMedalla(String tiempoDeEntregaMedalla) {
        this.tiempoDeEntregaMedalla = tiempoDeEntregaMedalla;
    }

    @Override
    public String toString() {
        return "Retos{" +
                "codigoReto=" + codigoReto +
                ", nombreReto='" + nombreReto + '\'' +
                ", descripcionReto='" + descripcionReto + '\'' +
                ", fotoDelReto='" + fotoDelReto + '\'' +
                ", cantidadDeKilometros=" + cantidadDeKilometros +
                ", medallaAsociada='" + medallaAsociada + '\'' +
                ", costoDelReto=" + costoDelReto +
                ", puntoInicioReto=" + puntoInicioReto +
                ", puntoFinReto=" + puntoFinReto +
                ", tiempoDeEntregaMedalla='" + tiempoDeEntregaMedalla + '\'' +
                '}';
    }
}
