package vargas.kennett.bl.entities.actividadFisica;

import java.util.ArrayList;

public interface ActividadFisicaDAO {

    public String registrarActividadFisica () throws Exception;
    public ArrayList <ActividadFisica> listarActividadFisica() throws Exception;
    public String modificarActividadFisica(ActividadFisica actividadFisica) throws Exception;
    public String eliminarActividadFisica(String codigo) throws Exception;


}
