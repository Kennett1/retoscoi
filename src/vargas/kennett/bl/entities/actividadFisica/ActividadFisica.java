package vargas.kennett.bl.entities.actividadFisica;

public class ActividadFisica {

    private double kilometrosARegistrar;
    private String tipoDeActividad;
    private String retirarse;

    public ActividadFisica() {
    }

    public ActividadFisica(double kilometrosARegistrar, String tipoDeActividad, String retirarse) {
        this.kilometrosARegistrar = kilometrosARegistrar;
        this.tipoDeActividad = tipoDeActividad;
        this.retirarse = retirarse;
    }

    public double getKilometrosARegistrar() {
        return kilometrosARegistrar;
    }

    public void setKilometrosARegistrar(double kilometrosARegistrar) {
        this.kilometrosARegistrar = kilometrosARegistrar;
    }

    public String getTipoDeActividad() {
        return tipoDeActividad;
    }

    public void setTipoDeActividad(String tipoDeActividad) {
        this.tipoDeActividad = tipoDeActividad;
    }

    public String getRetirarse() {
        return retirarse;
    }

    public void setRetirarse(String retirarse) {
        this.retirarse = retirarse;
    }

    @Override
    public String toString() {
        return "ActividadFisica{" +
                "kilometrosARegistrar=" + kilometrosARegistrar +
                ", tipoDeActividad='" + tipoDeActividad + '\'' +
                ", retirarse='" + retirarse + '\'' +
                '}';
    }
}
