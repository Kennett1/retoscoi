package vargas.kennett.bl.entities.usualVisual;

import java.util.Arrays;

public class UsuaVisual {

    private String [] listRetosParticipados;
    private String retoActual;
    private double porcentageActividad;


    public UsuaVisual() {
    }

    public UsuaVisual(String[] listRetosParticipados, String retoActual, double porcentageActividad) {
        this.listRetosParticipados = listRetosParticipados;
        this.retoActual = retoActual;
        this.porcentageActividad = porcentageActividad;
    }

    public String[] getListRetosParticipados() {
        return listRetosParticipados;
    }

    public void setListRetosParticipados(String[] listRetosParticipados) {
        this.listRetosParticipados = listRetosParticipados;
    }

    public String getRetoActual() {
        return retoActual;
    }

    public void setRetoActual(String retoActual) {
        this.retoActual = retoActual;
    }

    public double getPorcentageActividad() {
        return porcentageActividad;
    }

    public void setPorcentageActividad(double porcentageActividad) {
        this.porcentageActividad = porcentageActividad;
    }

    @Override
    public String toString() {
        return "UsuaVisual{" +
                "listRetosParticipados=" + Arrays.toString(listRetosParticipados) +
                ", retoActual='" + retoActual + '\'' +
                ", porcentageActividad=" + porcentageActividad +
                '}';
    }
}
