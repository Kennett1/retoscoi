package vargas.kennett.bl.entities.usualVisual;

import java.util.ArrayList;

public interface UsuarioVisualDao {
    public ArrayList<UsuaVisual> listarUsuarioVisual() throws Exception;
}
