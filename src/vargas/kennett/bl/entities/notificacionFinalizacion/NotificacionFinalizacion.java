package vargas.kennett.bl.entities.notificacionFinalizacion;

public class NotificacionFinalizacion {

    private String felicitacionSistema;
    private String felicitacionCorreo;

    public NotificacionFinalizacion() {
    }

    public NotificacionFinalizacion(String felicitacionSistema, String felicitacionCorreo) {
        this.felicitacionSistema = felicitacionSistema;
        this.felicitacionCorreo = felicitacionCorreo;
    }

    public String getFelicitacionSistema() {
        return felicitacionSistema;
    }

    public void setFelicitacionSistema(String felicitacionSistema) {
        this.felicitacionSistema = felicitacionSistema;
    }

    public String getFelicitacionCorreo() {
        return felicitacionCorreo;
    }

    public void setFelicitacionCorreo(String felicitacionCorreo) {
        this.felicitacionCorreo = felicitacionCorreo;
    }

    @Override
    public String toString() {
        return "NotificacionFinalizacion{" +
                "felicitacionSistema='" + felicitacionSistema + '\'' +
                ", felicitacionCorreo='" + felicitacionCorreo + '\'' +
                '}';
    }
}
