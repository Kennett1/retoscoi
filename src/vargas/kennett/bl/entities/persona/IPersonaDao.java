package vargas.kennett.bl.entities.persona;

import vargas.kennett.bl.entities.usuario.Usuario;

import java.util.ArrayList;

public interface IPersonaDao {
    public String registrarPersona(Persona persona) throws Exception;
    public ArrayList<Persona> listarPersonas() throws Exception;
    public String modificarPersona(Persona persona) throws Exception;
    public String eliminarPersona(String codigo) throws Exception;


}

