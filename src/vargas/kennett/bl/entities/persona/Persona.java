package vargas.kennett.bl.entities.persona;

public class Persona {

    private String nombre;
    private String apellido;
    private String identificacion;
    private String pais;
    private String email;
    private String contrasenna;

    public Persona(String nombre,  String apellido, String identificacion, String pais, String contrasenna) {
        this.nombre = "";
        this.apellido = "";
        this.identificacion = "";
        this.pais = "";
        email= "";
        this.contrasenna = "";
    }

    public Persona(String nombre, String apellido, String identificacion, String pais, String email, String contrasenna) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.identificacion = identificacion;
        this.pais = pais;
        this.email = email;
        this.contrasenna = contrasenna;
    }

    public Persona() {

    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContrasenna() {
        return contrasenna;
    }

    public void setContrasenna(String contrasenna) {
        this.contrasenna = contrasenna;
    }

    @Override
    public String toString() {
        return "Persona{" +
                "nombre='" + nombre + '\'' +
                ", apellido='" + apellido + '\'' +
                ", identificacion='" + identificacion + '\'' +
                ", pais='" + pais + '\'' +
                ", email='" + email + '\'' +
                ", contrasenna='" + contrasenna + '\'' +
                '}';
    }
}
