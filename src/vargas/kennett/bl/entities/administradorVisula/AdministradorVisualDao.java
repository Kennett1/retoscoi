package vargas.kennett.bl.entities.administradorVisula;

import vargas.kennett.bl.entities.actividadFisica.ActividadFisica;

import java.util.ArrayList;

public interface AdministradorVisualDao {

    public ArrayList<ActividadFisica> listarAdministradorVisual() throws Exception;
}
