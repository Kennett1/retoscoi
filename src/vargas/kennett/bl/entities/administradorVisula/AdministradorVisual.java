package vargas.kennett.bl.entities.administradorVisula;

import java.util.Arrays;

public class AdministradorVisual {

    private String [] listUsuarios;
    private String [] listRetos;

    public AdministradorVisual() {
    }

    public AdministradorVisual(String[] listUsuarios, String[] listRetos) {
        this.listUsuarios = listUsuarios;
        this.listRetos = listRetos;
    }

    public String[] getListUsuarios() {
        return listUsuarios;
    }

    public void setListUsuarios(String[] listUsuarios) {
        this.listUsuarios = listUsuarios;
    }

    public String[] getListRetos() {
        return listRetos;
    }

    public void setListRetos(String[] listRetos) {
        this.listRetos = listRetos;
    }

    @Override
    public String toString() {
        return "AdministradorVisual{" +
                "listUsuarios=" + Arrays.toString(listUsuarios) +
                ", listRetos=" + Arrays.toString(listRetos) +
                '}';
    }
}
