package vargas.kennett.bl.entities.notificacionActividad;

public class NotificacionActividad {

    private double kilometrosIngresados;
    private double kilometrosAcomulados;
    private double porcentageDeActividad;

    public NotificacionActividad() {
    }

    public NotificacionActividad(double kilometrosIngresados, double kilometrosAcomulados, double porcentageDeActividad) {
        this.kilometrosIngresados = kilometrosIngresados;
        this.kilometrosAcomulados = kilometrosAcomulados;
        this.porcentageDeActividad = porcentageDeActividad;
    }

    public double getKilometrosIngresados() {
        return kilometrosIngresados;
    }

    public void setKilometrosIngresados(double kilometrosIngresados) {
        this.kilometrosIngresados = kilometrosIngresados;
    }

    public double getKilometrosAcomulados() {
        return kilometrosAcomulados;
    }

    public void setKilometrosAcomulados(double kilometrosAcomulados) {
        this.kilometrosAcomulados = kilometrosAcomulados;
    }

    public double getPorcentageDeActividad() {
        return porcentageDeActividad;
    }

    public void setPorcentageDeActividad(double porcentageDeActividad) {
        this.porcentageDeActividad = porcentageDeActividad;
    }

    @Override
    public String toString() {
        return "NotificacionActividad{" +
                "kilometrosIngresados=" + kilometrosIngresados +
                ", kilometrosAcomulados=" + kilometrosAcomulados +
                ", porcentageDeActividad=" + porcentageDeActividad +
                '}';
    }
}
