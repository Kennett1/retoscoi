package vargas.kennett.bl.entities.usuario;

import java.util.ArrayList;

public interface IUsuarioDao {
    public String registrarUsuario (Usuario usuario) throws Exception;
    public ArrayList<Usuario> listarUsuario() throws Exception;
    public String modificarUsuario(Usuario usuario) throws Exception;
    public String eliminarUsuario(String codigo) throws Exception;
}
