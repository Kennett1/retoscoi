package vargas.kennett.bl.entities.usuario;

import vargas.kennett.bl.entities.persona.Persona;

import java.sql.Date;
import java.util.Calendar;

import java.util.GregorianCalendar;
import java.util.TimeZone;

public class Usuario extends Persona {

    private String avatarUsuario;
    private Date nacimientoUsuario;
    private int edadUsuario;
    private String generoUsuario;
    private String direccionUsuario;
    private String provinciaUsuario;

    public Usuario(String nombre, String apellido, String identificacion, String pais, String contrasenna, String avatarUsuario, Date nacimientoUsuario, String generoUsuario, String direccionUsuario, String provinciaUsuario) {
        super();
    }

    public Usuario(String nombre, String apellido, String identificacion, String pais, String email, String contrasenna,
                   String avatarUsuario, Date nacimientoUsuario, String generoUsuario, String direccionUsuario, String provinciaUsuario) {

        super (nombre, apellido, identificacion, pais, email, contrasenna);
        this.avatarUsuario = avatarUsuario;
        this.nacimientoUsuario = nacimientoUsuario;
        this.generoUsuario = generoUsuario;
        this.direccionUsuario = direccionUsuario;
        this.provinciaUsuario = provinciaUsuario;
        //this.edadUsuario=obtenerEdadUsuario(getNacimientoUsuario());

    }

    public String getAvatarUsuario() {
        return avatarUsuario;
    }

    public void setAvatarUsuario(String avatarUsuario) {
        this.avatarUsuario = avatarUsuario;
    }

    public Date getNacimientoUsuario() {
        return nacimientoUsuario;
    }

    public void setNacimientoUsuario(Date nacimientoUsuario) {
        this.nacimientoUsuario = nacimientoUsuario;
    }

    public int getEdadUsuario() {
        return edadUsuario;
    }


    public String getGeneroUsuario() {
        return generoUsuario;
    }

    public void setGeneroUsuario(String generoUsuario) {
        this.generoUsuario = generoUsuario;
    }

    public String getDireccionUsuario() {
        return direccionUsuario;
    }

    public void setDireccionUsuario(String direccionUsuario) {
        this.direccionUsuario = direccionUsuario;
    }

    public String getProvinciaUsuario() {
        return provinciaUsuario;
    }

    public void setProvinciaUsuario(String provinciaUsuario) {
        this.provinciaUsuario = provinciaUsuario;
    }

    /*public int obtenerEdadUsuario (String nacimientoUsuario) {

        Calendar fechaNacimiento = new GregorianCalendar(TimeZone.getDefault());
        Calendar ahora = Calendar.getInstance();

        long edadEnDias = (ahora.getTimeInMillis() - fechaNacimiento.getTimeInMillis())
                / 1000 / 60 / 60 / 24;

        int anos = Double.valueOf(edadEnDias / 365.25d).intValue();

return anos;
    }
*/
    @Override
    public String toString() {
        return "Usuario{" +
                "avatarUsuario='" + avatarUsuario + '\'' +
                ", nacimientoUsuario=" + nacimientoUsuario +
                ", edadUsuario=" + edadUsuario +
                ", generoUsuario='" + generoUsuario + '\'' +
                ", direccionUsuario='" + direccionUsuario + '\'' +
                ", ProvinciaUsuario='" + provinciaUsuario + '\'' +
                '}';
    }
}
