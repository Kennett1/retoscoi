package vargas.kennett.bl.entities.hito;



import java.util.ArrayList;

public interface HitoDao {

    public String registrarHito () throws Exception;
    public ArrayList<Hito> listarHito() throws Exception;
    public String modificarHito (Hito hito) throws Exception;
    public String eliminarHito(String codigo) throws Exception;
}
