package vargas.kennett.bl.entities.hito;

public class Hito {

    private String nombreHito;
    private double kilometroDeHito;
    private String provincia;
    private String distrito;
    private String canton;
    private double puntoInicioHito;
    private double puntoFinHito;
    private int identificadorDistrito;
    private int identificadorCanton;
    private int identificadorProvincia;
    private String imagenHito;

    public Hito() {
    }

    public Hito(String nombreHito, double kilometroDeHito, String provincia, String distrito, String canton, double puntoInicioHito, double puntoFinHito, int identificadorDistrito, int identificadorCanton, int identificadorProvincia, String imagenHito) {
        this.nombreHito = nombreHito;
        this.kilometroDeHito = kilometroDeHito;
        this.provincia = provincia;
        this.distrito = distrito;
        this.canton = canton;
        this.puntoInicioHito = puntoInicioHito;
        this.puntoFinHito = puntoFinHito;
        this.identificadorDistrito = identificadorDistrito;
        this.identificadorCanton = identificadorCanton;
        this.identificadorProvincia = identificadorProvincia;
        this.imagenHito = imagenHito;
    }

    public String getNombreHito() {
        return nombreHito;
    }

    public void setNombreHito(String nombreHito) {
        this.nombreHito = nombreHito;
    }

    public double getKilometroDeHito() {
        return kilometroDeHito;
    }

    public void setKilometroDeHito(double kilometroDeHito) {
        this.kilometroDeHito = kilometroDeHito;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getDistrito() {
        return distrito;
    }

    public void setDistrito(String distrito) {
        this.distrito = distrito;
    }

    public String getCanton() {
        return canton;
    }

    public void setCanton(String canton) {
        this.canton = canton;
    }

    public double getPuntoInicioHito() {
        return puntoInicioHito;
    }

    public void setPuntoInicioHito(double puntoInicioHito) {
        this.puntoInicioHito = puntoInicioHito;
    }

    public double getPuntoFinHito() {
        return puntoFinHito;
    }

    public void setPuntoFinHito(double puntoFinHito) {
        this.puntoFinHito = puntoFinHito;
    }

    public int getIdentificadorDistrito() {
        return identificadorDistrito;
    }

    public void setIdentificadorDistrito(int identificadorDistrito) {
        this.identificadorDistrito = identificadorDistrito;
    }

    public int getIdentificadorCanton() {
        return identificadorCanton;
    }

    public void setIdentificadorCanton(int identificadorCanton) {
        this.identificadorCanton = identificadorCanton;
    }

    public int getIdentificadorProvincia() {
        return identificadorProvincia;
    }

    public void setIdentificadorProvincia(int identificadorProvincia) {
        this.identificadorProvincia = identificadorProvincia;
    }

    public String getImagenHito() {
        return imagenHito;
    }

    public void setImagenHito(String imagenHito) {
        this.imagenHito = imagenHito;
    }

    @Override
    public String toString() {
        return "Hito{" +
                "nombreHito='" + nombreHito + '\'' +
                ", kilometroDeHito=" + kilometroDeHito +
                ", provincia='" + provincia + '\'' +
                ", distrito='" + distrito + '\'' +
                ", canton='" + canton + '\'' +
                ", puntoInicioHito=" + puntoInicioHito +
                ", puntoFinHito=" + puntoFinHito +
                ", identificadorDistrito=" + identificadorDistrito +
                ", identificadorCanton=" + identificadorCanton +
                ", identificadorProvincia=" + identificadorProvincia +
                ", imagenHito='" + imagenHito + '\'' +
                '}';
    }
}
