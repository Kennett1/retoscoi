package vargas.kennett.bl.entities.actividadDeportiva;

import vargas.kennett.bl.entities.actividadFisica.ActividadFisica;

import java.util.ArrayList;

public class ActividadDeportivaImplement implements ActividadDeportivaDao{


    @Override
    public String registrarActividadDeportiva() throws Exception {
        return null;
    }

    @Override
    public ArrayList<ActividadDeportiva> listarActividadDeportiva() throws Exception {
        return null;
    }

    @Override
    public String modificarActividadDeportiva(ActividadDeportiva actividadDeportiva) throws Exception {
        return null;
    }

    @Override
    public String eliminarActividadDeportiva(String codigo) throws Exception {
        return null;
    }
}
