package vargas.kennett.bl.entities.actividadDeportiva;

import java.util.ArrayList;

public interface ActividadDeportivaDao {

    public String registrarActividadDeportiva () throws Exception;
    public ArrayList<ActividadDeportiva> listarActividadDeportiva() throws Exception;
    public String modificarActividadDeportiva(ActividadDeportiva actividadDeportiva) throws Exception;
    public String eliminarActividadDeportiva(String codigo) throws Exception;
}
