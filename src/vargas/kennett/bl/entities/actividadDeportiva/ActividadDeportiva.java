package vargas.kennett.bl.entities.actividadDeportiva;

public class ActividadDeportiva {

    private int codigoActividad;
    private String nombreActividad;
    private String iconoActividad;

    public ActividadDeportiva() {
    }

    public ActividadDeportiva(int codigoActividad, String nombreActividad, String iconoActividad) {
        this.codigoActividad = codigoActividad;
        this.nombreActividad = nombreActividad;
        this.iconoActividad = iconoActividad;
    }

    public int getCodigoActividad() {
        return codigoActividad;
    }

    public void setCodigoActividad(int codigoActividad) {
        this.codigoActividad = codigoActividad;
    }

    public String getNombreActividad() {
        return nombreActividad;
    }

    public void setNombreActividad(String nombreActividad) {
        this.nombreActividad = nombreActividad;
    }

    public String getIconoActividad() {
        return iconoActividad;
    }

    public void setIconoActividad(String iconoActividad) {
        this.iconoActividad = iconoActividad;
    }

    @Override
    public String toString() {
        return "ActividadDeportiva{" +
                "codigoActividad=" + codigoActividad +
                ", nombreActividad='" + nombreActividad + '\'' +
                ", iconoActividad='" + iconoActividad + '\'' +
                '}';
    }
}
