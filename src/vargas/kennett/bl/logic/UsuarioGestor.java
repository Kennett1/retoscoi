package vargas.kennett.bl.logic;

import vargas.kennett.bl.entities.usuario.IUsuarioDao;
import vargas.kennett.bl.entities.usuario.Usuario;
import vargas.kennett.dao.DAOFactory;


import java.sql.Date;

public class UsuarioGestor
{

    private DAOFactory factory;
    private IUsuarioDao dao;

    public UsuarioGestor (){
        factory = DAOFactory.getDAOFactory();

        dao = factory.getUsuarioDao();
    }

    public String registrarUsuario(String nombre, String apellido, String identificacion, String pais, String email, String contrasenna,String avatarUsuario, Date nacimientoUsuario, String generoUsuario, String direccionUsuario, String provinciaUsuario) throws Exception {
        Usuario usuario=new Usuario(nombre,apellido,identificacion,pais,email,contrasenna, avatarUsuario, nacimientoUsuario, generoUsuario, direccionUsuario, provinciaUsuario);
        return dao.registrarUsuario(usuario);
    }
}
