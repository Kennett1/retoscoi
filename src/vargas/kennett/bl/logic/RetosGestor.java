package vargas.kennett.bl.logic;

import vargas.kennett.bl.entities.retos.IRetosDao;
import vargas.kennett.bl.entities.retos.MySqlIRetosImplement;
import vargas.kennett.bl.entities.retos.Retos;
import vargas.kennett.dao.DAOFactory;

public class RetosGestor {

    private DAOFactory factory;
    private IRetosDao dao;

    public RetosGestor (){
        factory = DAOFactory.getDAOFactory();

        dao = factory.get();
    }

    public String registrarRetos(String nombre, String apellido, String identificacion, String pais, String email, String contrasenna) throws Exception {
        Retos retos=new Retos(nombre,apellido,identificacion,pais,email,contrasenna);
        return dao.registrarRetos(retos);
    }

}
