package vargas.kennett.bl.logic;

import vargas.kennett.bl.entities.persona.IPersonaDao;
import vargas.kennett.bl.entities.persona.Persona;
import vargas.kennett.bl.entities.persona.MySqlIPersonaImplement;
import vargas.kennett.dao.DAOFactory;
import vargas.kennett.dl.Data;

import java.sql.SQLException;
import java.util.ArrayList;

public class InicioGestor {

    private String admin;
    private DAOFactory factory;
    private IPersonaDao dao;

    public InicioGestor (){
        factory = DAOFactory.getDAOFactory();

        dao = factory.getPersonaDao();
    }


    public InicioGestor(String admin) {
        this.admin = admin;
    }

    public String getAdmin() {
        return admin;
    }

    public void setAdmin(String admin) {
        this.admin = admin;
    }

    public boolean evaluacionAdmin () throws Exception {
        ArrayList<Persona> personas= dao.listarPersonas();

        if(personas.size()>0){ // si hay admin
            return true;
        }else{ // No hay admin
            return false;
        }

    }

    /*public boolean validarUsuarioExistente(String usuario,String contrasenna){
        ArrayList<Persona> usuariosAdmin=data.getAdmins();
        boolean result=false;
        for(int counter=0;counter<usuariosAdmin.size();counter++){
            Persona persona=usuariosAdmin.get(counter);
            if(persona.getEmail().equals(usuario)){
                if(persona.getContrasenna().equals(contrasenna)){
                    result= true;
                }
            }
        }
        return result;
    }
*/

}
