package vargas.kennett.bl.logic;

import vargas.kennett.bl.entities.persona.IPersonaDao;
import vargas.kennett.bl.entities.persona.Persona;
import vargas.kennett.bl.entities.persona.MySqlIPersonaImplement;
import vargas.kennett.dao.DAOFactory;
import vargas.kennett.dl.Data;

import java.sql.SQLException;
import java.util.ArrayList;

public class AdminGestor
{
    private DAOFactory factory;
    private IPersonaDao dao;

public AdminGestor (){
        factory = DAOFactory.getDAOFactory();

        dao = factory.getPersonaDao();
    }

    public String registrarPersona(String nombre, String apellido, String identificacion, String pais, String email, String contrasenna) throws Exception {
        Persona persona=new Persona(nombre,apellido,identificacion,pais,email,contrasenna);
        return dao.registrarPersona(persona);
    }

    public boolean validarUsuarioExistente(String usuario,String contrasenna) throws Exception {
        ArrayList<Persona> usuariosAdmin= dao.listarPersonas();
        boolean result=false;
        for(int counter=0;counter<usuariosAdmin.size();counter++){
            Persona persona=usuariosAdmin.get(counter);
            if(persona.getEmail().equals(usuario)){
                if(persona.getContrasenna().equals(contrasenna)){
                    result= true;
                }
            }
        }
        return result;
    }


    /*public String modificarCurso(String codigo, String nombre, int creditos) throws Exception{
        Curso curso = new Curso(codigo,nombre,creditos);
        return dao.modificarCurso(curso);
    }

    public String eliminarCurso(String codigo) throws Exception{
        return dao.eliminarCurso(codigo);
    }

     */

    public ArrayList<Persona> listarPersona() throws Exception {
        return dao.listarPersonas();
    }

}
