package vargas.kennett.dao;

import vargas.kennett.bl.entities.persona.IPersonaDao;
import vargas.kennett.bl.entities.usuario.IUsuarioDao;
import vargas.kennett.util.Utils;

import javax.rmi.CORBA.Util;

public abstract class DAOFactory {

    public static final int MYSQL=1;

    public static DAOFactory getDAOFactory(){
        try {

            String[] infoProperties = Utils.getProperties();
            int repository = Integer.parseInt(infoProperties[5]);

            switch (repository){
                case MYSQL:
                    return new MySqlDaoFactory();
                default: return null;
            }
        }catch(Exception e){
            return null;
        }
    }
    public abstract IPersonaDao getPersonaDao();
    public abstract IUsuarioDao getUsuarioDao();

}
