package vargas.kennett.dao;

import vargas.kennett.bl.entities.persona.IPersonaDao;
import vargas.kennett.bl.entities.persona.MySqlIPersonaImplement;
import vargas.kennett.bl.entities.usuario.IUsuarioDao;
import vargas.kennett.bl.entities.usuario.MySqlIUsuarioImplement;

public class MySqlDaoFactory extends DAOFactory{

    public IPersonaDao getPersonaDao() {
        return new MySqlIPersonaImplement();
    }

    public IUsuarioDao getUsuarioDao() {
        return new MySqlIUsuarioImplement();
    }
}
