package vargas.kennett.tl;

import vargas.kennett.bl.logic.AdminGestor;
import vargas.kennett.bl.logic.InicioGestor;
import vargas.kennett.ui.*;

import java.io.IOException;

public class Controller {

 private UI interfaze;
    private InicioGestor inicioGestor;
    private AdminGestor adminGestor;

    public Controller (){
        interfaze = new UI();
        inicioGestor = new InicioGestor();
        adminGestor=new AdminGestor();
    }



    public void start () throws Exception {
        int option;
            interfaze.selectionTipeUser ();
            option = interfaze.chooseOption();
            processOption(option);

    }

    public void processOption (int option) throws Exception {
        switch (option){
            case 1: // Type admin
                boolean result=inicioGestor.evaluacionAdmin();
                if(result==false){ // No admin users
                    System.out.print("No existen usuarios administradores, bienvenido al registro de usuarios");
                    interfaze.printMessage("Ingrese su nombre");
                    String nombre= interfaze.readText();
                    interfaze.printMessage("Ingrese su apellido");
                    String apellido=interfaze.readText();
                    interfaze.printMessage("Ingrese su identificacion");
                    String identificacion=interfaze.readText();
                    interfaze.printMessage("Ingrese su pais");
                    String pais=interfaze.readText();
                    interfaze.printMessage("Ingrese su email");
                    String email=interfaze.readText();
                    interfaze.printMessage("Ingrese una contrasena de usuario");
                    String contrasenna=interfaze.readText();

                    adminGestor.registrarPersona(nombre,apellido,identificacion,pais,email,contrasenna);

                    interfaze.printMessage("El usuario administrador ha sido creado");
                }else{ // Si hay admin user
                    System.out.print("Ingrese su usuario");
                    String usuario=interfaze.readText();

                    System.out.print("Ingrese su contrasenna");
                    String constrasenna=interfaze.readText();

                    boolean resultadoExistente=adminGestor.validarUsuarioExistente(usuario,constrasenna);




                }
                break;
            case 2: // Type deportista
               // inicioAdmin.evalucionDeportista("deportista");
                break;
            case 3: // Salida
                interfaze.printMessage("Esperamos seguir ayudandole a realizar deporte");
                break;
            default:
                interfaze.printMessage("Porfavor escoja una opcion valida");
        }
    }


}
